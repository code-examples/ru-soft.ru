# [ru-soft.ru](https://ru-soft.ru) source codes

<br/>

### Run ru-soft.ru on localhost

    # vi /etc/systemd/system/ru-soft.ru.service

Insert code from ru-soft.ru.service

    # systemctl enable ru-soft.ru.service
    # systemctl start ru-soft.ru.service
    # systemctl status ru-soft.ru.service

http://localhost:4062
